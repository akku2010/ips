import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Language } from './app.model';

@Injectable()
export class AppLanguages {

  private _lang: Language[] = [
    new Language(this.translate.instant('English'), 'en'),
    new Language(this.translate.instant('Hindi'), 'hi'),
    new Language(this.translate.instant('Marathi'), 'mr'),
    new Language(this.translate.instant('Bangali'), 'bn'),
    new Language(this.translate.instant('Tamil'), 'ta'),
    new Language(this.translate.instant('Telugu'), 'te'),
    new Language(this.translate.instant('Gujarati'), 'gu'),
    new Language(this.translate.instant('Kannada'), 'kn'),
    new Language(this.translate.instant('Malayalam'), 'ml'),
    new Language(this.translate.instant('Spanish'), 'sp'),
    new Language(this.translate.instant('Persian'), 'fa'),
    new Language(this.translate.instant('Arabic'), 'ar'),
    new Language(this.translate.instant('Nepali'), 'ne'),
    new Language(this.translate.instant('French'), 'fr'),
    new Language(this.translate.instant('Punjabi'), 'pn'),
    new Language(this.translate.instant('Portuguese'), 'pt'),
    new Language(this.translate.instant('Pashto'), 'ps'),
    new Language(this.translate.instant('Italian'), 'it'),
    new Language(this.translate.instant('Dutch'), 'nl'),
    new Language(this.translate.instant('Albanian'), 'alb'),
  ];

  constructor(
    private translate: TranslateService
  ) { }

  get Languages() {
    return [...this._lang];
  }
}
