import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@IonicPage()
@Component({
  selector: 'page-fastag-list',
  templateUrl: 'fastag-list.html',
})
export class FastagListPage implements OnInit {
  islogin: any;
  fastagList: any[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private apiCall: ApiServiceProvider
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter FastagListPage');
  }

  ngOnInit() {
    this.getList();
  }

  addFastag() {
    this.navCtrl.push('FastagPage');
  }

  getList() {
    var url = this.apiCall.mainUrl + 'fastTag/getRequest?id=' + this.islogin.supAdmin + '&role=supAdmin';
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(url)
      .subscribe(respData => {
        this.apiCall.stopLoading();
        console.log('respData: ', respData);
        if (respData.length > 0) {
          this.fastagList = respData;
        }
      },
        err => {
          this.apiCall.stopLoading();
        })
  }

}
