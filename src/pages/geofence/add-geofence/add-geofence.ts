import { ApiServiceProvider } from "../../../providers/api-service/api-service";
import { AlertController, ToastController, NavController, IonicPage, ViewController, Events, Platform } from "ionic-angular";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { GoogleMaps, GoogleMap, GoogleMapsEvent, CameraPosition, Marker, LatLng, ILatLng, Polygon, Circle, Polyline, MyLocation, GoogleMapsAnimation } from '@ionic-native/google-maps';
import { NativeGeocoder, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
// import { Geolocation } from '@ionic-native/geolocation';
declare var google;

@IonicPage()
@Component({
  selector: 'page-add-geofence',
  templateUrl: './add-geofence.html',
})
export class AddGeofencePage implements OnInit {
  map: GoogleMap;
  mapElement: HTMLElement;
  geofenceForm: FormGroup;
  submitAttempt: boolean;
  geofencedetails: any;
  finalcordinate: any = [];
  islogin: any;
  devicesadd: any;
  curLat: any; curLng: any;
  entering: any;
  exiting: any;
  cord: any = [];
  isdevice: string;
  markers: any;
  acService: any;
  autocompleteItems: any = [];
  autocomplete: any = {};
  newLat: any;
  newLng: any;
  storedLatLng: any = [];
  latlat: number;
  lnglng: number;

  fence: number = 200;
  circleData: Circle;
  collectPoly: any;
  polyineArray: any[] = [];
  keepMark: any[] = [];
  liveMark: any;

  constructor(
    public apiCall: ApiServiceProvider,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public fb: FormBuilder,
    public navCtrl: NavController, 
    // public geoLocation: Geolocation,
    public viewCtrl: ViewController,
    private nativeGeocoder: NativeGeocoder,
    public events: Events,
    public plt: Platform
  ) {
    //console.log("maptest", this.map);
    //console.log("maptest1", this.map.moveCamera);
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.geofenceForm = fb.group({
      geofence_name: [''],
      check: [false],
      check1: [false]
    });

    this.acService = new google.maps.places.AutocompleteService();

  }

  ngOnInit() {
    // this.runGeofenceMaps();
    this.drawGeofence();
    this.autocompleteItems = [];
    this.autocomplete = {
      query: '',
      creation_type: 'circular'
    };
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  enteringFunc() {
    console.log(this.geofenceForm.value.check);
    this.entering = this.geofenceForm.value.check;
  }

  exitingFunc() {
    console.log(this.geofenceForm.value.check1);
    this.exiting = this.geofenceForm.value.check1;
  }

  creategoefence() {
    let that = this;
    that.submitAttempt = true;

    // console.log("circle coordinates: ", that.circleData.getCenter().lat);
    // console.log("circle radius: ", that.circleData.getRadius());
    if (that.autocomplete.creation_type == 'polygon') {
      for (var r = 0; r < that.storedLatLng.length; r++) {
        var a = [];
        a.push(parseFloat(that.storedLatLng[r].lng));
        a.push(parseFloat(that.storedLatLng[r].lat));
        that.cord.push(a);
      }
      that.finalcordinate.push(that.cord);
      if (this.geofenceForm.valid) {
        if (this.entering || this.exiting) {

          if (this.geofenceForm.value.geofence_name && that.finalcordinate.length) {

            var data = {
              "uid": this.islogin._id,
              "geoname": this.geofenceForm.value.geofence_name,
              "entering": this.entering,
              "exiting": this.exiting,
              "geofence": that.finalcordinate
            }
            this.apiCall.startLoading().present();
            this.apiCall.addgeofenceCall(data)
              .subscribe(data => {
                this.apiCall.stopLoading();
                this.devicesadd = data;
                console.log(this.devicesadd);

                let toast = this.toastCtrl.create({
                  message: 'Geofence created successfully',
                  position: 'bottom',
                  duration: 2000
                });

                toast.onDidDismiss(() => {
                  console.log('Dismissed toast');
                  this.events.publish('reloadDetails');
                  this.navCtrl.pop()
                });

                toast.present();
              }, err => {
                this.apiCall.stopLoading();
                let alert = this.alertCtrl.create({
                  message: 'Please draw valid geofence..',
                  buttons: [{
                    text: 'OK', handler: () => {
                      that.storedLatLng = [];
                      that.finalcordinate = [];
                      that.cord = [];
                      this.drawGeofence();
                    }
                  }]
                });
                alert.present();
                console.log(err);
              });
          } else {

            let toast = this.toastCtrl.create({
              message: 'Select Geofence On Map or make sure fence name field is not empty!',
              position: 'top',
              duration: 2000
            });

            toast.present();
          }
        } else {
          let alert = this.alertCtrl.create({
            message: 'All fields are required!',
            buttons: ['Try Again']
          });
          alert.present();
        }
      }
    } else if (that.autocomplete.creation_type == 'circular') {
      if (this.geofenceForm.valid) {
        if (this.entering || this.exiting) {
          if (this.geofenceForm.value.geofence_name && that.circleData != undefined) {

            var payload = {
              "poi": [
                {
                  "location": {
                    "type": "Point",
                    "coordinates": [
                      that.circleData.getCenter().lng,
                      that.circleData.getCenter().lat
                    ]
                  },
                  "poiname": that.geofenceForm.value.geofence_name,
                  "status": "Active",
                  "user": that.islogin._id,
                  "address": "N/A",
                  "radius": that.circleData.getRadius()
                }
              ]
            };
            this.apiCall.startLoading().present();
            this.apiCall.addPOIAPI(payload)
              .subscribe(data => {
                console.log("response adding poi: ", data)
                this.apiCall.stopLoading();
                this.events.publish('reloadDetails');
                this.navCtrl.pop();
                let toast = this.toastCtrl.create({
                  message: "Geofence created successfully!",
                  duration: 1500,
                  position: 'top'
                });
                toast.present();

              },
                err => {
                  console.log("error adding poi: ", err);
                  this.apiCall.stopLoading();
                })
          } else {

            let toast = this.toastCtrl.create({
              message: 'Select Geofence On Map or make sure fence name field is not empty!',
              position: 'top',
              duration: 2000
            });

            toast.present();
          }
        } else {
          let alert = this.alertCtrl.create({
            message: 'All fields are required!',
            buttons: ['Try Again']
          });
          alert.present();
        }
      }
    }
  }

  updateSearch() {
    // debugger
    console.log('modal > updateSearch');
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    let that = this;
    let config = {
      //types:  ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
      input: that.autocomplete.query,
      componentRestrictions: {}
    }
    this.acService.getPlacePredictions(config, function (predictions, status) {
      console.log('modal > getPlacePredictions > status > ', status);
      console.log("lat long not find ", predictions);

      that.autocompleteItems = [];
      predictions.forEach(function (prediction) {
        that.autocompleteItems.push(prediction);
      });
      console.log("autocompleteItems=> " + that.autocompleteItems)
    });
  }

  chooseItem(item) {
    let that = this;
    that.autocomplete.query = item.description;
    console.log("console items=> " + JSON.stringify(item))
    that.autocompleteItems = [];

    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };

    this.nativeGeocoder.forwardGeocode(item.description, options)
      .then((coordinates: NativeGeocoderForwardResult[]) => {
        console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude)
        that.newLat = coordinates[0].latitude;
        that.newLng = coordinates[0].longitude;
        let pos: CameraPosition<ILatLng> = {
          target: new LatLng(that.newLat, that.newLng),
          zoom: 15,
          tilt: 30
        };
        this.map.moveCamera(pos);
        this.map.addMarker({
          title: '',
          position: new LatLng(that.newLat, that.newLng),
        }).then((data) => {
          console.log("Marker added")
        })

      })
      .catch((error: any) => console.log(error));

  }

  callThis(fence) {
    let that = this;
    that.circleData.setRadius(fence);
  }

  radioChecked(key) {
    let that = this;
    console.log("radio btn checked: ", key)
    that.autocomplete.creation_type = key;
    if (key == "polygon") {
      if (that.circleData) {
        that.circleData.remove();
        that.fence = 200;
      }
      that.storedLatLng = [];

    } else {
      if (key == "circular") {

        // this.geoLocation.getCurrentPosition().then((resp) => {
        //   this.latlat = resp.coords.latitude;
        //   this.lnglng = resp.coords.longitude
        //   let pos: CameraPosition<ILatLng> = {
        //     target: new LatLng(resp.coords.latitude, resp.coords.longitude),
        //     zoom: 16,
        //     tilt: 30
        //   };
        //   this.map.moveCamera(pos);
        //   if (that.circleData != undefined) {
        //     that.circleData.remove();
        //   }

        //   let ltln: ILatLng = { "lat": this.latlat, "lng": this.lnglng };
        //   let circle: Circle = this.map.addCircleSync({
        //     'center': ltln,
        //     'radius': that.fence,
        //     'strokeColor': '#d80622',
        //     'strokeWidth': 2,
        //     'fillColor': 'rgb(255, 128, 128, 0.5)'
        //   });

        //   that.circleData = circle;
        //   console.log("circle data: => " + that.circleData)

        // });

        // Get the location of you
        this.map.getMyLocation()
          .then((location: MyLocation) => {
            console.log(JSON.stringify(location, null, 2));
            this.latlat = location.latLng.lat;
            this.lnglng = location.latLng.lng
            // Move the map camera to the location with animation
            this.map.animateCamera({
              target: location.latLng,
              zoom: 17,
              tilt: 30
            }).then(() => {
              if (that.circleData != undefined) {
                that.circleData.remove();
              }

              let ltln: ILatLng = { "lat": this.latlat, "lng": this.lnglng };
              let circle: Circle = this.map.addCircleSync({
                'center': ltln,
                'radius': that.fence,
                'strokeColor': '#d80622',
                'strokeWidth': 2,
                'fillColor': 'rgb(255, 128, 128, 0.5)'
              });

              that.circleData = circle;
              console.log("circle data: => " + that.circleData)
              // this.map.addMarker({
              //   title: 'Add POI here(Drag Me)',
              //   position: location.latLng,
              //   animation: GoogleMapsAnimation.BOUNCE,
              //   draggable: true
              // }).then((marker: Marker) => {
              //   this.markerObj = marker;
              //   marker.on(GoogleMapsEvent.MARKER_DRAG_END)
              //     .subscribe(() => {
              //       this.markerObj = undefined;
              //       this.markerObj = marker;
              //     });
              // })
            });
          });

        if (that.collectPoly) {
          that.collectPoly.remove();
        }
        if (that.polyineArray.length > 0) {
          for (var i = 0; i < that.polyineArray.length; i++) {
            that.polyineArray[i].remove();
          }
        }

        if (that.keepMark.length > 0) {
          for (var r = 0; r < that.keepMark.length; r++) {
            that.keepMark[r].remove();
          }
        }
        that.storedLatLng = [];
      }
    }
  }

  drawGeofence() {
    if (this.map != undefined) {
      this.map.remove();
    }
    this.mapElement = document.getElementById('mapGeofence');
    // this.map = this.googleMaps.create(this.mapElement);
    this.map = GoogleMaps.create(this.mapElement);
    // Wait the MAP_READY before using any methods.

    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        console.log('Map is ready!');
        let that = this;
        if (that.liveMark) {
          that.liveMark.remove();
        }

        this.map.getMyLocation()
        .then((location: MyLocation) => {
          console.log(JSON.stringify(location, null, 2));
          this.latlat = location.latLng.lat;
          this.lnglng = location.latLng.lng
          // Move the map camera to the location with animation
          this.map.animateCamera({
            target: location.latLng,
            zoom: 17,
            tilt: 30
          }).then(() => {
            this.map.addMarker({
              title: '',
              position: new LatLng(location.latLng.lat, location.latLng.lng),
            }).then((marker: Marker) => {
              console.log("Marker added")
              that.liveMark = marker;
            })
  
            if (that.autocomplete.creation_type == 'circular') {
              if (that.circleData != undefined) {
                that.circleData.remove();
              }
              // that.storedLatLng.push(data[0]);
              // Add circle
              let ltln: ILatLng = { "lat": location.latLng.lat, "lng": location.latLng.lng };
              let circle: Circle = this.map.addCircleSync({
                'center': ltln,
                'radius': that.fence,
                'strokeColor': '#d80622',
                'strokeWidth': 2,
                'fillColor': 'rgb(255, 128, 128, 0.5)'
              });
  
              that.circleData = circle;
              console.log("circle data: => " + that.circleData)
  
            }
          });
        });

        that.map.on(GoogleMapsEvent.MAP_CLICK).subscribe(
          (data) => {
            if (that.autocomplete.creation_type == 'circular') {
              if (that.circleData != undefined) {
                that.circleData.remove();
              }
              // that.storedLatLng.push(data[0]);
              // Add circle
              let ltln: ILatLng = { "lat": data[0].lat, "lng": data[0].lng };
              let circle: Circle = this.map.addCircleSync({
                'center': ltln,
                'radius': that.fence,
                'strokeColor': '#d80622',
                'strokeWidth': 2,
                'fillColor': 'rgb(255, 128, 128, 0.5)'
              });

              that.circleData = circle;
              console.log("circle data: => " + that.circleData)
              // this.map.moveCamera({
              //   target: circle.getBounds()
              // });
            } else {
              if (that.autocomplete.creation_type == 'polygon') {
                var markicon;
                if (that.plt.is('ios')) {
                  markicon = 'www/assets/imgs/circle1.png';
                } else if (that.plt.is('android')) {
                  markicon = './assets/imgs/circle1.png';
                }
                that.storedLatLng.push(data[0]);
                that.map.addMarker({
                  position: data[0],
                  icon: {
                    url: markicon,
                    size: {
                      width: 13,
                      height: 13
                    }
                  }
                }).then((mark: Marker) => {
                  that.keepMark.push(mark);
                  mark.on(GoogleMapsEvent.MARKER_CLICK).subscribe((latLng: LatLng) => {
                    that.storedLatLng.push(that.storedLatLng[0])  // store first lat lng at last also
                    let GORYOKAKU_POINTS: ILatLng[] = that.storedLatLng;
                    that.map.addPolygon({
                      'points': GORYOKAKU_POINTS,
                      'strokeColor': '#000',
                      'fillColor': '#ffff00',
                      'strokeWidth': 5
                    }).then((polygon: Polygon) => {
                      // console.log("GORYOKAKU_POINTS=> " + JSON.stringify(GORYOKAKU_POINTS));
                      that.collectPoly = polygon;
                    });
                  });
                });
                debugger
                // console.log(JSON.stringify(that.storedLatLng));
                if (that.storedLatLng.length > 1) {
                  that.map.addPolyline({
                    points: that.storedLatLng,
                    color: '#000000',
                    width: 3,
                    geodesic: true
                  }).then((poly: Polyline) => {
                    that.polyineArray.push(poly);
                  })
                }
              }
            }
          }
        );
      });
  }
}
