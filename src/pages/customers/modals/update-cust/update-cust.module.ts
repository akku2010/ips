import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateCustModalPage } from './update-cust';
import { TranslateModule } from '@ngx-translate/core';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    UpdateCustModalPage
  ],
  imports: [
    IonicPageModule.forChild(UpdateCustModalPage),
    TranslateModule.forChild(),
    SelectSearchableModule
  ]
})
export class UpdateCustModalPageModule {}
