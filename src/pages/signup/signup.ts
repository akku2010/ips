import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, Platform, Keyboard, Select } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceProvider } from '../../providers/api-service/api-service';


@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  signupForm: FormGroup;
  isdealer: boolean;
  submitAttempt: boolean;
  usersignupdetails: any;
  signupUseradd: any;
  responseMessage: string;
  signupDetails: any;
  type1 = "password";

  countryCodeArray: any = [];
  conCode: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public apiService: ApiServiceProvider,
    private toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public platform: Platform,
    public keyboard: Keyboard,

  ) {
    this.getCountryCode();
    // this.signupForm = formBuilder.group({
    //   mob_num: ["", Validators.compose([Validators.minLength(10), Validators.maxLength(13), Validators.required])],
    //   email_add: ["", Validators.email],
    //   Name: ["", Validators.required],
    //   pass: ["", Validators.required],
    //   cnfrm_passwrd: ["", Validators.required],
    //   std_code: ["", Validators.required]
    //   // mob_num: ['', [Validators.required, Validators.minLength(10)]]
    //   // selectedCode: [""]
    // });
    this.signupForm = formBuilder.group({
      mob_num: [null, Validators.compose([Validators.minLength(10), Validators.maxLength(13)])],
      email_add: [null],
      Name: [null, Validators.required],
      pass: [null, Validators.required],
      cnfrm_passwrd: [null, Validators.required],
      lName: [null, Validators.required],
      userId: [null, Validators.required],
      std_code: [null]
    })
  }

  getCountryCode() {
    this.apiService.getCountryCode().subscribe(data => {
      this.countryCodeArray = data.countries;
    })
  }

  onSelect(con) {
    console.log("country selected: ", con)
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter SignupPage');
  }

  selectedCountryCode = 'us';
  countryCodes = ['us', 'lu', 'de', 'bs', 'br', 'pt'];

  changeSelectedCountryCode(value: string): void {
    this.selectedCountryCode = value;
  }

  doLogin() {
    this.navCtrl.setRoot("LoginPage");
  }

  IsDealer(check) {
    this.isdealer = check;
  }

  // getotp() {
  //   debugger
  //   this.isdealer = false;
  //   this.submitAttempt = true;
  //   if (this.signupForm.valid) {
  //     this.usersignupdetails = this.signupForm.value;
  //     localStorage.setItem('usersignupdetails', this.usersignupdetails);
  //     this.signupDetails = localStorage.getItem("usersignupdetails");
  //     if (this.signupForm.value.cnfrm_passwrd && this.signupForm.value.email_add && this.signupForm.value.mob_num && this.signupForm.value.Name && this.signupForm.value.pass) {
  //       if (this.signupForm.value.pass == this.signupForm.value.cnfrm_passwrd) {

  //         var usersignupdata = {
  //           "first_name": this.signupForm.value.Name,
  //           "last_name": '',
  //           "email": this.signupForm.value.email_add,
  //           "password": this.signupForm.value.pass,
  //           "confirmpass": this.signupForm.value.cnfrm_passwrd,
  //           "phone": String(this.signupForm.value.mob_num),
  //           "status": false,
  //           "supAdmin": "59cbbdbe508f164aa2fef3d8",
  //           "isDealer": false,
  //           "std_code": this.conCode
  //         }

  //         this.apiService.startLoading();
  //         this.apiService.signupApi(usersignupdata)
  //           .subscribe(response => {
  //             var phone = usersignupdata.phone;
  //             localStorage.setItem("mobnum", phone)
  //             this.apiService.stopLoading();
  //             this.signupUseradd = response;
  //             let toast = this.toastCtrl.create({
  //               message: response.message,
  //               duration: 3000,
  //               position: 'top'
  //             });

  //             toast.onDidDismiss(() => {
  //               if (response.message === 'Email ID or Mobile Number already exists') {
  //                 this.navCtrl.push("LoginPage");
  //               } else if (response.message === "OTP sent successfully") {
  //                 this.navCtrl.push('SignupOtpPage');
  //               }
  //             });
  //             toast.present();
  //           },
  //             err => {
  //               this.apiService.stopLoading();
  //               let toast = this.toastCtrl.create({
  //                 message: "Something went wrong. Please check your net connection..",
  //                 duration: 2500,
  //                 position: "top"
  //               })
  //               toast.present();
  //             })
  //       } else {
  //         var alertPopup = this.alertCtrl.create({
  //           title: 'Warning!',
  //           message: "Password and Confirm Password Not Matched",
  //           buttons: ['OK']
  //         });
  //         alertPopup.present();
  //       }
  //     }
  //   }
  // }

  getotp() {
    debugger
    function validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }
    this.isdealer = false;
    this.submitAttempt = true;
    if (this.signupForm.valid) {
      this.usersignupdetails = this.signupForm.value;
      localStorage.setItem('usersignupdetails', this.usersignupdetails);
      this.signupDetails = localStorage.getItem("usersignupdetails");
      // if (this.signupForm.value.cnfrm_passwrd && this.signupForm.value.email_add && this.signupForm.value.mob_num && this.signupForm.value.Name && this.signupForm.value.pass) {
      // if (this.signupForm.value.cnfrm_passwrd && this.signupForm.value.email_add && this.signupForm.value.Name && this.signupForm.value.pass) {
      if (this.signupForm.value.email_add !== null) {

        var isEmail = validateEmail(this.signupForm.value.email_add);
        if (!isEmail) {
          let toast = this.toastCtrl.create({
            message: 'Please enter valid email address and try again',
            duration: 3000,
            position: 'top'
          });
          toast.present();
          return;
        }
      }
      if (this.signupForm.value.mob_num !== null) {

        if (this.signupForm.value.std_code == null) {
          this.toastCtrl.create({
            message: 'Please select the country code.',
            duration: 3000,
            position: 'middle'
          }).present();
          return;
        }
      }
      debugger
      if (this.signupForm.value.pass === this.signupForm.value.cnfrm_passwrd) {

        var usersignupdata = {
          "first_name": this.signupForm.value.Name,
          "last_name": this.signupForm.value.lName,
          "password": this.signupForm.value.pass,
          "org_name": null,
          "email": (this.signupForm.value.email_add ? this.signupForm.value.email_add : null),
          "phone": (this.signupForm.value.mob_num ? this.signupForm.value.mob_num : null),
          // "expdate": "2021-04-02T06:56:41.356Z",
          "supAdmin": "59cbbdbe508f164aa2fef3d8",
          "isDealer": false,
          "custumer": true,
          "user_id": this.signupForm.value.userId,
          "imageDoc": [],
          "std_code": (this.signupForm.value.std_code ? this.signupForm.value.std_code : null)
        }
        this.apiService.startLoading();
        this.apiService.signupApi(usersignupdata)
          .subscribe(response => {
            debugger
            this.apiService.stopLoading();
            if (usersignupdata.phone !== null) {
              var phone = usersignupdata.phone;
              localStorage.setItem("mobnum", phone)

              this.signupUseradd = response;

            }

            let toast = this.toastCtrl.create({
              message: response.message,
              duration: 3000,
              position: 'top'
            });

            toast.onDidDismiss(() => {
              if (response.message === 'Email ID or Mobile Number already exists') {
                this.navCtrl.push("LoginPage");
              } else if (response.message === "OTP sent successfully") {
                this.navCtrl.push('SignupOtpPage');
              } else if (response.message === "Registered Sucessfully") {
                this.navCtrl.push("LoginPage");
              }
            });
            toast.present();

          },
            err => {
              this.apiService.stopLoading();
              let toast = this.toastCtrl.create({
                message: "Something went wrong. Please check your net connection..",
                duration: 2500,
                position: "top"
              })
              toast.present();
            })
      } else {
        var alertPopup = this.alertCtrl.create({
          title: 'Warning!',
          message: "Password and Confirm Password Not Matched",
          buttons: ['OK']
        });
        alertPopup.present();
      }
      // }
    }
  }

  gotoOtp() {
    this.navCtrl.push('SignupOtpPage');
  }

  gotoLogin() {
    this.navCtrl.push("LoginPage");
  }

  type = "password";
  show = false;
  show1 = false;
  toggleShow(ev) {
    if (ev == 0) {
      this.show = !this.show;
      if (this.show) {
        this.type = "text";
      }
      else {
        this.type = "password";
      }
    } else {
      this.show1 = !this.show1;
      if (this.show1) {
        this.type1 = "text";
      }
      else {
        this.type1 = "password";
      }
    }
  }

  upload() {
    this.navCtrl.push("DrivingLicensePage");
  }

  goBack() {
    this.navCtrl.pop();
  }

}
